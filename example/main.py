from machine import Pin, PWM
import uencoder

encoder_pin_A = 27
encoder_pin_B = 26

# Exponential steps, offset +45
pwm_steps = ((0,500), (5,500), (55,500), (220,1000), (520,1000), (1023,1000))
max_step = const(5)
global cur_step
cur_step = 0

# LED pin settings
led_pin = const(4)


def cb_encoder(direction):
    global cur_step
    if direction == uencoder.DIR_CCW:
        # CCW rotation
        print("Turned COUNTER-CLOCKWISE")
        if cur_step > 0:
            cur_step -= 1
            print("Set step to {}".format(cur_step))
    elif direction == uencoder.DIR_CW:
        # CW rotation
        print("Turned CLOCKWISE")
        if cur_step < max_step:
            cur_step += 1
            print("Set step to {}".format(cur_step))


def main():
    pin_A = Pin(encoder_pin_A, Pin.IN)
    pin_B = Pin(encoder_pin_B, Pin.IN)
    encoder = uencoder.Encoder(pin_A=pin_A, pin_B=pin_B, callback=cb_encoder)
    led = PWM(Pin(led_pin), freq=1000, duty=0)

    set_step = 0
    global cur_step
    while True:
        if cur_step != set_step:
            led = PWM(Pin(led_pin), freq=pwm_steps[cur_step][1], duty=pwm_steps[cur_step][0])
            set_step = cur_step


if __name__ == '__main__':
    main()
