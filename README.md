# uEncoder

Simple MicroPython library for reading from a rotary encoder. 

Based on C++ code originally by Ben Buxton, available [on GitHub](https://github.com/buxtronix/arduino/tree/master/libraries/Rotary), licensed under GPLv3.


## Installation

Installing is easy - just copy the `uencoder.py` file to your MicroPython board. It can live either in the root, or in a `/lib` folder. 

The file is only a few kilobytes, but for more efficiency it can be frozen into a MicroPython binary image - just place the file inside the `ports/<board>/modules` folder when building MicroPython from source, then flash to the board as usual. 


## Usage

Using the library is pretty simple. First, there needs to be a function to be used as the callback - for example: 

```python
def cb_encoder(direction):
    if direction == uencoder.DIR_CCW:
        # CCW rotation
        print("Turned COUNTER-CLOCKWISE")
    elif direction == uencoder.DIR_CW:
        # CW rotation
        print("Turned CLOCKWISE")
```

The callback will be called with either `DIR_CW` or `DIR_CCW` as an argument. Action should then be taken as desired. 

Once that is defined, the `uEncoder` class can be instantiated: 

```python
from machine import Pin
import uencoder

def main():
    pin_A = Pin(encoder_pin_A, Pin.IN)
    pin_B = Pin(encoder_pin_B, Pin.IN)
    encoder = uencoder.Encoder(pin_A=pin_A, pin_B=pin_B, callback=cb_encoder)
    ...
```

See [`example/main.py`](./example/main.py). 

That's it! Note that if necessary, it should be possible to use internal pull-ups on the encoder pins if needed, with something like `pin_A = Pin(0, Pin.IN, Pin.PULL_UP)`. 


## License

uEncoder, a MicroPython library for reading rotary encoders

Copyright (C) 2018, Sean Lanigan

Based on C++ code originally by Ben Buxton, available on GitHub: 
https://github.com/buxtronix/arduino/tree/master/libraries/Rotary

This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more details. 

See [LICENSE](./LICENSE) for the full text. 
